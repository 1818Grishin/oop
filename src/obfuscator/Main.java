package obfuscator;

import java.util.Scanner;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException{
        Scanner read = new Scanner(System.in);
        System.out.print("Введите путь - ");
        String path = read.nextLine();
        FileObfuscator file = new FileObfuscator(path);

        file.obfuscate();
    }
}
