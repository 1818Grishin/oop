package obfuscator;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("all")

// Single comment for testing

/*
Multi comment for testing
 */

public class FileObfuscator {
    private String path;

    public FileObfuscator(String path) {
        this.path = path;
    }

    void obfuscate() throws IOException {
        String code = "";
        String nameFile = findFileName(path);
        String newPath = newFilePath(path);
        code = readFile(code, path);
        code = createNewClassName(code, nameFile);
        code = deleteComments(code);
        code = deleteSpaces(code);
        writeNewFile(code, newPath, nameFile);
    }

    public static String readFile(String s, String path) throws IOException {
        List<String> strings = Files.readAllLines(Paths.get(path));
        for (int i = 0; i < strings.size(); i++) {
            s = s.concat(strings.get(i));
        }
        return s;
    }

    public static String createNewClassName(String string, String nameFile) {
        return string.replaceAll(nameFile, "New" + nameFile);
    }

    public static String findFileName(String path) {
        ArrayList<String> strings = new ArrayList<>(Arrays.asList(path.split("/")));
        ArrayList<String> fileNames = new ArrayList<>(Arrays.asList(strings.get(strings.size() - 1).split("\\.")));
        String nameFile = fileNames.get(0);
        return nameFile;
    }

    private static String deleteSpaces(String stringFile) {
        return stringFile.replaceAll("\\s+(?![^\\d\\s])", "");
    }

    private static String deleteComments(String stringFile) {
        Pattern pattern = Pattern.compile("(\\/\\*.+?\\*\\/)|(\\/\\/.+?)[:;a-zA-Zа-яА-ЯЁё\\s]*", Pattern.DOTALL | Pattern.COMMENTS);
        Matcher matcher = pattern.matcher(stringFile);
        while (matcher.find()) {
            stringFile = stringFile.replace(matcher.group(), "");
        }
        return stringFile;
    }

    public static String newFilePath(String path) {
        String newFilePath = "";
        ArrayList<String> strings = new ArrayList<>(Arrays.asList(path.split("/")));
        for (int i = 0; i < strings.size() - 1; i++) {
            newFilePath = newFilePath + strings.get(i) + "/";
        }
        return newFilePath;
    }

    private static void writeNewFile(String stringFile, String path, String nameFile) throws IOException {
        try (FileWriter writer = new FileWriter(path + "New" + nameFile + ".java")) {
            writer.write(stringFile);
        }
    }
}