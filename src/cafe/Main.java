package cafe;
import cafe.products.Component;
import cafe.products.IceCream;
import cafe.products.Smoothie;
import cafe.products.Yogurt;
import cafe.toppings.SprinkingTopping;
import cafe.toppings.SyrupTopping;
import cafe.toppings.WithoutTopping;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Component ice;
        Component yogurt;
        Component smoothies;
        Random random = new Random();
        boolean randomFlag = random.nextBoolean();

        if (randomFlag) {
            randomFlag = random.nextBoolean();
            if (randomFlag) {
                ice = new SprinkingTopping(new IceCream());
            } else {
                ice = new SyrupTopping(new IceCream());
            }
        } else {
            ice = new IceCream();
        }

        randomFlag = random.nextBoolean();
        if (randomFlag) {
            randomFlag = random.nextBoolean();
            if (randomFlag) {
                smoothies = new SprinkingTopping(new Smoothie());
            } else {
                smoothies = new SyrupTopping(new Smoothie());
            }
        } else {
            smoothies = new Smoothie();
        }

        randomFlag = random.nextBoolean();
        if (randomFlag) {
            yogurt = new WithoutTopping(new Yogurt());
        } else {
            yogurt = new Yogurt();
        }

        ice.topping();
        smoothies.topping();
        yogurt.topping();

    }
}
