package cafe.toppings;
import cafe.products.Component;

public class WithoutTopping extends Decorator {
    public WithoutTopping(Component component) {
        super(component);
    }

    @Override
    public void afterTopping() {
        System.out.println(" - без топпинга");
    }
}
