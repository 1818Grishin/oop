package cafe.toppings;
import cafe.products.Component;

public class SprinkingTopping extends Decorator {
    public SprinkingTopping(Component component) {
        super(component);
    }

    @Override
    public void afterTopping() {
        System.out.println(" - добавлен топпинг-посыпка");
    }
}
