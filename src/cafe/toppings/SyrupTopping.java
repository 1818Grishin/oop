package cafe.toppings;
import cafe.products.Component;

public class SyrupTopping extends Decorator {
    public SyrupTopping(Component component) {
        super(component);
    }

    @Override
    public void afterTopping() {
        System.out.println(" - добавлен топпинг-сироп");
    }
}
