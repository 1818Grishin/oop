package cafe.toppings;
import cafe.products.Component;

public abstract class Decorator implements Component {
    private Component component;

    Decorator(Component component) {
        this.component = component;
    }

    public abstract void afterTopping();

    @Override
    public void topping() {
        component.topping();
        afterTopping();
    }
}