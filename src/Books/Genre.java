package Books;

public enum Genre {
    DETECTIVE,
    HORROR,
    FANTASY,
    CLASSICS,
    ADVENTURE,
    ACTION;
}
