package Books;

public class Books {
    private String bookName;
    private Genre genre;
    private int releaseYear;
    private String author;
    private String path;

    public Books (String bookName, Genre genre, int releaseYear, String author, String path) {
        this.bookName = bookName;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.author = author;
        this.path = path;
    }

    public String getBookName() {
        return bookName;
    }

    public Genre getGenre() {
        return genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public String getAuthor() {
        return author;
    }

    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return "Book{" + "bookName = " + bookName +
                ", genre = " + genre +
                ", releaseYear = " + releaseYear +
                ", author = " + author +
                ", path = " + path +
                '}';
    }
}
