package Books;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Books book1 = new Books("Убийца", Genre.DETECTIVE, 2016, "Фредерик Дар", "Z:/books/Detective" );
        Books book2 = new Books("Проклятье Вендиго", Genre.HORROR, 2017, "ACT", "Z:/books/Horror");
        Books book3 = new Books("Война и мир", Genre.CLASSICS, 1998, "Лев Толстой", "Z:/books/Classics");
        Books book4 = new Books("Демон рода Шосе", Genre.FANTASY, 2003, "Viktor", "Z:/books/Fantasy");
        ArrayList<Books> arraylist = new ArrayList<>(Arrays.asList(book1, book2, book3, book4));
        ArrayList<Books> authorBooks = new ArrayList<>();
        ArrayList<Books> genreBooks = new ArrayList<>();

        print(arraylist);

        bookSorting(arraylist);
        print(arraylist);

        releaseYearSorting(arraylist);
        print(arraylist);

        bookSearching(arraylist);
        System.out.println();

        authorFilter(arraylist, authorBooks);
        print(authorBooks);

        genreFilter(arraylist, genreBooks);
        print(genreBooks);
    }

    private static ArrayList bookSorting(ArrayList<Books> arraylist) {
        Books temp;

        for (int i = 0; i < arraylist.size(); i++) {
            for (int j = 1+i; j < arraylist.size(); j++) {
                int compare = arraylist.get(j).getAuthor().compareTo(arraylist.get(i).getAuthor());
                if (compare < 0) {
                    temp = arraylist.get(i);
                    arraylist.set(i, arraylist.get(j));
                    arraylist.set(j, temp);
                }

                if (compare == 1) {
                    if (arraylist.get(j).getReleaseYear() < arraylist.get(i).getReleaseYear()) {
                        temp = arraylist.get(i);
                        arraylist.set(i, arraylist.get(j));
                        arraylist.set(j, temp);
                    }
                }
            }
        }
        return arraylist;
    }

    private static ArrayList releaseYearSorting (ArrayList<Books> arraylist) {
        Books temp;

        for (int i = 0; i < arraylist.size(); i++) {
            for (int j = i + 1; j < arraylist.size(); j++) {
                if (arraylist.get(j).getReleaseYear() > arraylist.get(i).getReleaseYear()) {
                    temp = arraylist.get(i);
                    arraylist.set(i, arraylist.get(j));
                    arraylist.set(j, temp);
                }
            }
        }
        return arraylist;
    }

    private static void bookSearching (ArrayList<Books> arraylist) {
        Scanner read = new Scanner(System.in);
        System.out.print("Введите название книги - ");
        String book = read.nextLine();
        int counter = 0;
        for (int i = 0; i < arraylist.size(); i++) {
            if (book.equals(arraylist.get(i).getBookName())) {
                System.out.println(arraylist.get(i));
                counter++;
            }
        }
        if (counter == 0) {
            System.out.println("Книга с введенным названием отсутствует.");
        }
    }

    private static ArrayList authorFilter(ArrayList<Books> arraylist, ArrayList<Books> authorBooks) {
        Scanner read = new Scanner(System.in);
        System.out.print("Введите фамилию требуемого автора - ");
        String author = read.nextLine();
        for (int i = 0; i < arraylist.size(); i++) {
            if (arraylist.get(i).getAuthor().equals(author)) {
                authorBooks.add(arraylist.get(i));
            }
        }
        if (authorBooks.isEmpty()) {
            System.out.println("Книг данного автора нет или введены неверные данные.");
        }
        return authorBooks;
    }

    private static ArrayList genreFilter(ArrayList<Books> arrayList, ArrayList<Books> genreBooks) {
        Scanner read = new Scanner(System.in);
        System.out.print("Введите жанр требуемой книги - ");
        String genre = read.nextLine();
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).getGenre().toString().equals(genre)) {
                genreBooks.add(arrayList.get(i));
            }
        }
        if (genreBooks.isEmpty()) {
            System.out.println("Книги по введенному жанру отсутствуют или введены неверные данные.");
        }
        return genreBooks;
    }

    private static void print(ArrayList<Books> arraylist) {
        for (int i = 0; i < arraylist.size(); i++) {
            System.out.println(arraylist.get(i));
        }
        System.out.println();
    }
}